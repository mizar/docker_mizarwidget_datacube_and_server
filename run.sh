#!/usr/bin/env bash
WORKSPACE_PATH=$PWD/datacube_workspace

if docker run  -e LOCAL_USER_ID=`id -u $USER` --name mizarwidget-datacube_and_server \
            -d \
            -p 8000:80 \
            -p 8081:8081 \
            -v $WORKSPACE_PATH:/data \
            idocias/mizarwidget-datacube-and-server:latest 
then 
    sleep 15
    echo "Please launch : http://localhost:8000/"
fi

